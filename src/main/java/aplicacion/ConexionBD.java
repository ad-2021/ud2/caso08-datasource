package aplicacion;

import java.sql.Connection;
import java.sql.SQLException;

import org.mariadb.jdbc.MariaDbDataSource;
import org.postgresql.ds.PGSimpleDataSource;

// Patrón singleton
public class ConexionBD {

	private static Connection con = null;
	private static String IP = "192.168.56.102";

	public static Connection getConexion() throws SQLException {
		if (con == null) {
			// Instanciamos el objeto DataSource
			// Mariadb
			MariaDbDataSource ds = new MariaDbDataSource(IP, 3306, "empresa_ad");
			ds.setUrl("jdbc:mariadb://" + IP + ":3306/empresa_ad");

			// Postgresql
//			PGSimpleDataSource ds = new PGSimpleDataSource();
//			ds.setUrl("jdbc:postgresql://" + IP + ":5432/batoi?currentSchema=empresa_ad");

			ds.setUser("batoi");
			ds.setPassword("1234");
//			ds.setDatabaseName("empresa_ad");
//			ds.setDatabaseName("batoi"); // postgresql
//			ds.setCurrentSchema("empresa_ad"); // solo postgresql
//			ds.setPortNumber(3306); // solo mariadb
//			ds.setServerName(IP);	// solo mariadb
			
			con = ds.getConnection();
//			con = ds.getConnection("batoi", "1234");

		}
		return con;
	}

	public static void cerrar() throws SQLException {
		if (con != null) {
			con.close();
			con = null;
		}
	}

}
