package aplicacion;

import java.sql.Connection;
import java.sql.SQLException;
import org.mariadb.jdbc.MariaDbPoolDataSource;
import org.postgresql.ds.PGConnectionPoolDataSource;

public class ConexionBDPool {
	private static MariaDbPoolDataSource pds;
//	private static PGConnectionPoolDataSource pds;
	private static String IP = "192.168.56.102";

	public static Connection getConexion() throws SQLException {
		if (pds == null) {
			pds = new MariaDbPoolDataSource(IP, 3306, "empresa_ad");
			pds.setUrl("jdbc:mariadb://" + IP + ":3306/empresa_ad");
			pds.setServerName(IP);
			pds.setDatabaseName("empresa_ad");
			pds.setPortNumber(3306);
			pds.setUser("batoi");
			pds.setPassword("1234");
			pds.setMaxPoolSize(3);

//			pds = new PGConnectionPoolDataSource();
//			pds.setUrl("jdbc:postgresql://" + IP + ":5432/batoi?currentSchema=empresa_ad");
//			pds.setDatabaseName("batoi");
//			pds.setCurrentSchema("empresa_ad)");
//			pds.setUser("batoi");
//			pds.setPassword("1234)");
//			pds.setMaxPoolSize(3); // no disponible

		}
		return pds.getConnection();
	}

	public static void liberarConexion(Connection con) throws SQLException {
		if (con != null) {
			con.close();
		}
	}

	public static void cerrar() {
		if (pds != null) {
			pds.close();
			pds = null;
		}
	}

}
