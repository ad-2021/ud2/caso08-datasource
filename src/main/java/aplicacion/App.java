package aplicacion;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author sergio
 */
public class App {

	public static void main(String[] args) {
		Connection con;
		// Ejemplo con DataSource
		try {
			con = ConexionBD.getConexion();
			System.out.println("Conexión ok: " + con);

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				ConexionBD.cerrar();
			} catch (SQLException e) {
				System.err.println(e.getMessage());
			}
		}

		System.out.println("----------------------------------------------");
		Connection con1, con2, con3, con4;
		// Ejemplo con Pool: realizamos varias conexiones y liberamos.
		try {
			con1 = ConexionBDPool.getConexion();
			System.out.println("Conexion1 ok: " + con1);

			con2 = ConexionBDPool.getConexion();
			System.out.println("Conexion2 ok: " + con2);

			con3 = ConexionBDPool.getConexion();
			System.out.println("Conexion3 ok: " + con3);

			ConexionBDPool.liberarConexion(con1);

			con4 = ConexionBDPool.getConexion();
			System.out.println("Conexion4 ok: " + con4);

			ConexionBDPool.liberarConexion(con1);
			ConexionBDPool.liberarConexion(con2);
			ConexionBDPool.liberarConexion(con3);
			ConexionBDPool.liberarConexion(con4);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			ConexionBDPool.cerrar();
		}

	}

}
